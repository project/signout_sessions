CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration
 * Important URLs


INTRODUCTION
------------

Current Maintainer: Irfan Ahmed <irfworld@gmail.com>

This module allows site users to sign out from all other sessions if the 
sessions exist. The session for current logged in user remains.

Usage example:
Drupal allows its users to login with same account on different browsers or 
machines / devices at a same time.
This modules helps the current logged in user to sign out from all the other 
sessions. You can manage link position on profile page by setting up link weight
on "Account Settings Manage Display Page".

INSTALLATION
------------

* Install as you normally install a contributed Drupal module. 
  for further information See:
  https://drupal.org/documentation/install/modules-themes/modules-7


CONFIGURATION
-------------
Administrator or users having administrator permission, can manage settings for 
signout sessions. Please follow below steps:

Step1: Enable the module.
Step2: Go to Signout Sessions configuration page 
"/admin/config/people/signout-sessions".
Step3: Choose your preferred role(s) / Set link text / Enable or Disable log / 
provide wrapper class.
Step4: Save configuration.
Step5: Go to manage display page for user profile 
"/admin/config/people/accounts/display".
Step6: Set weight for the field "Signout Link".
Step7: Save.


Important URLs
-------------
To manage configuration, go to
URL: /admin/config/people/signout-sessions

To manage link position on profile page, go to
URL: /admin/config/people/accounts/display

To give administrative permission for signout session setting page, go to
URL: /admin/people/permissions#module-signout_sessions
