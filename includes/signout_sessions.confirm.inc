<?php

/**
 * @file
 * Provides signout sessions confirm form and submit handler.
 */

/**
 * Provides confirmation form for signout.
 *
 * See signout_sessions_confirm_submit for more details.
 */
function signout_sessions_confirm($form, &$form_state) {

  global $user;
  $form['id'] = array(
    '#type' => 'value',
    '#value' => 'signout_sessions_confirm',
  );
  return confirm_form($form,
    t('Are you sure you want to signout from all other sessions?'), 'user/' . $user->uid,
    t('This action cannot be undone.'),
    t('Sign out'),
    t('Cancel'));
}

/**
 * Submit handler for sign out.
 */
function signout_sessions_confirm_submit($form, &$form_state) {

  global $user;
  if ($form_state['values']['op'] == 'Sign out') {
    // Get user's session count and remove others if more than 1.
    if (signout_sessions_count($user->uid) > 1) {
      // Call the method to delete all the session except the current one.
      $resp = signout_sessions($user->uid, $user->sid);
      // Get the response and show notification.
      if ($resp) {
        drupal_set_message(t('You have been sign out successfully from all other sessions.'));
      }
      else {
        drupal_set_message(t('Unable to sign out'), 'error');
      }

      // Redirect back to profile.
      $form_state['redirect'] = 'user/' . $user->uid;
    }
    else {
      drupal_set_message(t('No other session found!!!'), 'error');
    }

  }
}
