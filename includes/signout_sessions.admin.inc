<?php

/**
 * @file
 * Provides signout setting form, user profile view alterations.
 */

/**
 * Provides signout sessions setting form.
 */
function signout_sessions_admin_form() {

  // Get all roles.
  $options = user_roles();
  // Remove Anonymous role.
  unset($options[1]);
  // Field for link text.
  $form['signout_sessions_link_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Singout Link Text'),
    '#required' => TRUE,
    '#default_value' => variable_get('signout_sessions_link_text', 'Sign out from all other sessions'),
  );
  // Field for link wrapper class.
  $form['signout_sessions_wrapper_class'] = array(
    '#type' => 'textfield',
    '#title' => t('Singout Wrapper Class'),
    '#default_value' => variable_get('signout_sessions_wrapper_class', ''),
  );
  // Checkboxes for User roles except anonymous.
  $form['signout_sessions_roles'] = array(
    '#title' => t('User Roles'),
    '#type' => 'checkboxes',
    '#description' => t('Select the role(s), you want to show signout link'),
    '#options' => $options,
    '#default_value' => variable_get('signout_sessions_roles', array()),
  );
  // Checkbox for signout logs.
  $form['signout_sessions_log'] = array(
    '#title' => t('Enable logging'),
    '#type' => 'checkbox',
    '#description' => t('Check this box to store user sign out from other sessions activity'),
    '#default_value' => variable_get('signout_sessions_log', 0),
  );
  return system_settings_form($form);
}
